

const items = [1, 2, 3, 4, 5, 5];

const each = require("../each");

function cb(item, index, arr) {
  return item + 2;
}

const result = each(items, cb);
console.log(result);
