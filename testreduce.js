const items = [1, 2, 3, 4, 5, 5];

const reduce = require("../reduce");

function cb(first, second) {
  return first + second;
}

const startValue = 4;

const result = reduce(items, cb, startValue);
console.log(result)
