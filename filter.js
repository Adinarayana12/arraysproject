function filter(elements, cb) {
    if ((!Array.isArray(elements)) || (!elements) || (!cb)) {
      return [];
    }
    let array = [];
    for (let i = 0; i < elements.length; i++) {
      let item = elements[i];
      let filtering = cb(item, i, elements);
      if (filtering) {
        array.push(item);
      }
    }
    return array;
  }
  
  module.exports = filter;
  
  

