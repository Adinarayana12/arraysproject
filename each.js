function each(elements, cb) {
  if (!Array.isArray(elements) || !cb || !elements) {
    return [];
  }
  for (let i = 0; i < elements.length; i++) {
   let item = elements[i];
    elements[i] = cb(item, i, elements);
  }
  return elements;
}

module.exports = each;
