let flattenFn = require("../flatten.js")

let array = [1, [2], [[3]], [[[4]]]]


let result = flattenFn(array)

console.log(result)