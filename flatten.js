function flattenFunction(array, depth = Infinity) {
    if (!array || !Array.isArray(array)) {
      
    }
    
    let count = 0;
    do {
      let modifiedArray = [];
      isNested = false;
      for (let item of array) {
        if (Array.isArray(item)) {
          modifiedArray.push(...item);
          isNested = true;
        } else if (item === undefined) {
          continue;
        } else {
          modifiedArray.push(item);
        }
      }
      count++;
      array = modifiedArray;
      if (count === depth || !isNested) {
        break;
      } else {
        continue;
      }
    } while (isNested);
    return array;
  }
  
  module.exports = flattenFunction;
  
