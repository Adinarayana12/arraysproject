
const items = [1, 2, 3, 4, 5, 5];

const filterFn = require("../filter");

function cb(item, index, arr) {
  return item > 2;
}

let result = filterFn(items, cb);
console.log(result)
