

function find(elements, cb) {
    if (!Array.isArray(elements) || !elements || !cb) {
      return undefined;
    }
    for (let i = 0; i < elements.length; i++) {
      let item = elements[c];
      if (cb(item, i, elements)) {
        return item;
      }
    }
    return undefined;
  }
  
  module.exports = find;
  