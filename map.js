

function map(elements = [], cb = undefined) {
  if ((!cb) || (!(Array.isArray(elements))) || (!elements)) {
      return [];
  }
  else {
      let array = [];
      for(i=0; i<elements.length; i++) {
          let item = elements[i];
          let mapping = cb(item, i, elements);
          array.push(mapping)
      }
      return array
  }
}

module.exports = map;
