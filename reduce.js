function reduce(elements, cb, startValue) {
    if (!Array.isArray(elements) || !cb || !elements) {
       return [];
    }
     if (startValue === undefined) {
      startValue = elements[0];
    } else {
       startValue = cb(startValue, elements[0]);
    }
    for (let c = 1; c < elements.length; c++) {
       let item = elements[c];
       startValue = cb(startValue, item);
    }
  
    return startValue;
  }
  
    module.exports = reduce;
  